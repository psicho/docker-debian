## Project info

NOTE: In Settings > CI/CD > Pipeline triggers, trigger "Build when dotfiles has been updated" revoked due to quota limitations

![](https://gitlab.com/psicho/docker-debian/badges/master/pipeline.svg?key_text=Debian%20build&key_width=90)

Dockerfile for Debian image with basic applications and dotfiles

## Build
```
docker build -t debian-toolbox https://gitlab.com/psicho/docker-debian.git
```

## Push Image to docker hub
```
docker login
docker tag debian-toolbox psicho/debian-toolbox
docker push psicho/debian-toolbox
```

## Push Image to GitLab Registry
Will be done automatically via GitLab CI.

## Run
From local tag:
```
docker run -dit --name mydebian debian-toolbox
```
From Docker Hub:
```
docker run -dit --name mydebian psicho/debian-toolbox
```
From GitLab registry:
```
docker run -dit --name mydebian registry.gitlab.com/psicho/docker-debian
```

## Login
```
docker exec -it mydebian
```
(defaults to bash) or
```
docker exec -it mydebian zsh
```

## Known issues
 * yq and lazygit can't be installed due to a ppa/launchpad problem with it's key
