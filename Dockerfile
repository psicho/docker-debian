FROM debian

USER root

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get upgrade --yes

# prevent 'debconf: delaying package configuration, since apt-utils is not installed'
# one warning will occur though when apt-utils is being installed
RUN apt-get install --yes apt-utils

RUN apt-get install --yes \
        bzip2 \
        cron \
        curl \
        # nslookup, dig, host
        dnsutils \
        git \
        htop \
        # traceroute
        inetutils-traceroute \
        # lsmod is not available in containers - need this workaround
        kmod \
        less \
        lsb-release \
        lsof \
        man \
        nano \
        # ifconfig
        net-tools \
        # ps
        procps \
        # pstree
        psmisc \
        sudo \
        telnet \
        tcpflow \
        vim \
        wget \
    && \
    apt-get clean && \
    rm --recursive --force /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir /home/me && \
    useradd --home-dir /home/me --shell /bin/bash me && \
    chown me:me /home/me && \
    echo "me ALL=NOPASSWD: ALL" >> /etc/sudoers

USER me

# install dotfiles
RUN cd /home/me && \
    curl --location --remote-name https://vipc.de/bootstrap && \
    TERM=xterm-256color bash bootstrap --silent --name mydebian --all --java-version 17

WORKDIR /home/me

# afterwards, change passwords with passwd
